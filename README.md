# SAM

SAM.
Requirement:
Anaconda 3-5.2.0 - https://repo.anaconda.com/archive/.

Start on Windows: run setup.bat -> START.bat.

Start on Mac:
- setup:
```
conda create -n sam python=3.6 --y
pip install -r mac_requirements.txt
```
- run:
```
source activate sam
python debugger.py
```
Issue & Fix
1. 'import cv2' fails because numpy.core.multiarray fails to import: `pip install numpy==1.18.5`
2. Webcam black screen: https://stackoverflow.com/questions/29645278/webcam-open-cv-python-black-screen. Current solution:
```
video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
```

