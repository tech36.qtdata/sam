import PySimpleGUI as sg
import os, sys, io
import subprocess, psutil, signal
import pydoc
from apiclient.http import MediaIoBaseDownload

import util

    

def hello(text = 'Hello, world!'):
    return text


def hi():
    return 'Hi there!'

              
def get_filepath(filename_extension = 'all'):
    try:
        if filename_extension == 'xlsx':
            path = sg.PopupGetFile(
                       'Choose File to Upload',
                       no_window = True,
                       default_extension = '.xlsx',
                       file_types = (('Excel Files', '*.xlsx'),)
                   )
        elif filename_extension == 'py':
            path = sg.PopupGetFile(
                       'Choose File to Upload',
                       no_window = True,
                       multiple_files = True,
                       default_extension = '.py',
                       file_types = (('Python Files', '*.py'),)
                   )
        else:
            if sys.platform == 'win32':
                path = sg.PopupGetFile(
                    'Choose File to Upload',
                    no_window = True,
                    multiple_files = True,
                    default_extension = '*.*',
                    file_types = (('ALL Files', '*.*'),)
                )
            else:
                path = sg.PopupGetFile(
                    'Choose File to Upload',
                    no_window = True,
                    multiple_files = True,
                    default_extension = '*.*',
                    file_types = (
                        ('RTF Files', '*.rtf'),
                        ('Text Files', '*.txt'),
                        ('Python Files', '*.py'),
                        ('Excel Files', '*.xlsx'),
                    )
                )
    except:
        path = None
    return path


def get_folderpath():
    try:
        path = sg.PopupGetFolder('Select a Destination', no_window = True)
    except:
        path = None
    return path


def whatis(module):
    return pydoc.render_doc(module, 'help(%s)', renderer = pydoc.plaintext)


def list_spreadsheet_by_folderid(folder_id):
    results = util.drive_service.files().list(q = "mimeType='application/vnd.google-apps.spreadsheet' and parents in '"\
        + folder_id + "' and trashed = false", fields = "nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    
    return items


def list_file_by_folderid(folder_id):
    results = util.drive_service.files().list(q = "mimeType!='application/vnd.google-apps.folder' and parents in '"\
        + folder_id + "' and trashed = false", fields = "nextPageToken, files(id, name)", pageSize = 400).execute()
    items = results.get('files', [])
        
    return items 


def list_folder_by_folderid(folder_id):
    results = util.drive_service.files().list(q = "mimeType='application/vnd.google-apps.folder' and parents in '"\
        + folder_id + "' and trashed = false", fields = "nextPageToken, files(id, name)", pageSize = 400).execute()
    items = results.get('files', [])
        
    return items      


def get_file_by_fileid(file_dict, path):
    request = util.drive_service.files().get_media(fileId = file_dict['id'])
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
    
    with io.open(path + file_dict['name'], 'wb') as f:
        fh.seek(0)
        f.write(fh.read())


def execute_command(display_name, command, *args, communicate = False):      
    try:
#        print(command + ' ' + ' '.join(list(args)))
        p = psutil.Popen(
            command + ' ' + ' '.join(list(args)),
            shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE
        )      

        if communicate:      
            out, err = p.communicate()      
            if out:      
                print(out.decode('utf-8'))      
            if err:      
                print(err.decode('utf-8'))
                
        print('{} start: {}'.format(display_name, p.pid))
        
    except Exception as e:
        print(e)
        pass
    
    return p.pid


def kill_processtree(display_name, pid, sig = signal.SIGTERM, include_parent = True, timeout = None, on_terminate = None):
    """
    Kill a process tree (including grandchildren) with signal "sig".
    "on_terminate", if specified, is a callabck function which is called as soon as a child terminates.
    """
    try:
        if pid == os.getpid():
            raise RuntimeError("I refuse to kill myself")
        parent = psutil.Process(pid)
        children = parent.children(recursive = True)
        if include_parent:
            children.append(parent)
        for p in children:
            p.send_signal(sig)
        
        print('{} kill: {}'.format(display_name, pid))
        
    except Exception as e:
        print(e)
        pass          