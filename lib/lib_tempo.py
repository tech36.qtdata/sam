import PySimpleGUI as sg
import traceback
import os, sys
import json
from datetime import datetime
from googleapiclient.http import MediaFileUpload

import util
from lib import lib_sys
from logger import log_debugger



def checkin():
    if util.temponote_pid is not None:
        lib_sys.kill_processtree('TempoNote', util.temponote_pid)
        util.temponote_pid = None
    util.temponote_pid = lib_sys.execute_command(
                        'TempoNote',
                        'python', './popup_tempo.py', '-n on')
    if util.capimgs_pid is not None:
        lib_sys.kill_processtree('Capture Images', util.capimgs_pid)
        util.capimgs_pid = None
    util.capimgs_pid = lib_sys.execute_command(
                        'Capture Images',
                        'python', './popup_tempo.py', '-c on')
    
    return 'Start TempoNote: {}\nStart Capture Images: {}'.format(util.temponote_pid, util.capimgs_pid)


def checkout():
    try:
        lib_sys.kill_processtree('TempoNote', util.temponote_pid)
        lib_sys.kill_processtree('Capture Images', util.capimgs_pid)
        if util.tempomonitor_pid is not None:
            lib_sys.kill_processtree('TempoMonitor', util.tempomonitor_pid)
    except:
        pass
    
    return 'Stop TempoNote: {}\nStop Capture Images: {}'.format(util.temponote_pid, util.capimgs_pid)


def run_monitor():
    if util.tempomonitor_pid is not None:
        lib_sys.kill_processtree('TempoMonitor', util.tempomonitor_pid)
        util.tempomonitor_pid = None
    util.tempomonitor_pid = lib_sys.execute_command(
                        'TempoMonitor',
                        'python', './popup_tempo.py', '-m on')
    
    return 'Start TempoMonitor: {}'.format(util.tempomonitor_pid)


def create_window_todo():
    sg.theme('TanBlue')
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', 15))
        
    layout = [
        [sg.Text('Today Outcomes')],
        [sg.Multiline(key = '-OUTCOMES-', size = (45, 5))],
        [sg.Text('To Do')],
        [sg.Multiline(key = '-TODO-', size = (45, 5))],
        [sg.Submit(key = '-SUBMIT-')]
    ]
    
    window = sg.Window(
        'To Do List',
        layout,
        keep_on_top = True,
        finalize = True
    )
    
    return window
    

def add_todo():
    window = create_window_todo()

    while True:
        try:
            event, values = window.read(timeout = 100)
            if event == sg.TIMEOUT_KEY:
                tp_outcomes = values['-OUTCOMES-']
                tp_todo = values['-TODO-']
                pass

            if len(tp_outcomes.replace('\n','')) > 0 and len(tp_todo.replace('\n','')) > 0 and event == '-SUBMIT-':
                todo_path = util.path_note + 'todo_{}_{}.json'\
                    .format(util.username, datetime.now().strftime('%d-%b-%y').upper())
                if not os.path.exists(todo_path):
                    with open(todo_path, 'w+') as file:
                        json.dump([], file)
                    json_todo = []
                else:
                    #Load json data     
                    with open(todo_path, 'r') as file:
                        json_todo = json.load(file)
                dict_todo = {
                    'DATETIME': datetime.now().strftime('%d-%b-%y').upper(),
                    'TIME': datetime.now().strftime('%H:%M:%S'),
                    'NAME': util.username,
                    'OUTCOMES': tp_outcomes,
                    'TODO': tp_todo
                }
                #Write to json
                json_todo.append(dict_todo)
                with open(todo_path, 'w') as file:
                    json.dump(json_todo, file)
                        
                window.Hide()
                window.Close()
                
                return 'Todo list submitted successfully.'
            
            if event == sg.WIN_CLOSED:
                window.Close()
                break
    
        except UnicodeDecodeError:
            pass     
        
        except KeyboardInterrupt:
            pass
                        
        except:
            log_debugger.warning(traceback.format_exc())
            window.Close()
            return 'Todo list submitted failed!'


def get_tempo_folder(username = util.username):
    tempo_id = ''
    temp_id = ''
    
    list_folder = lib_sys.list_folder_by_folderid(util.tempodata_id)
    for idx, val in enumerate(list_folder):
        if val['name'] == username:
            tempo_id = val['id']
            break
    
    list_user_folder = lib_sys.list_folder_by_folderid(tempo_id)
    for idx, val in enumerate(list_user_folder):
        if val['name'] == 'temp_{}'.format(username):
            temp_id = val['id']
            break
    
    return tempo_id, temp_id


def push_file(filename, path, folder_id):
    exist_flag = 0
    
    #Create metadata and content
    file_metadata = {'name': filename,
                     'parents': [folder_id]}
    media = MediaFileUpload(path,
                            mimetype = '*/*',
                            resumable = True)
    #Check if file is exist
    list_file = lib_sys.list_file_by_folderid(folder_id)
    for idx, val in enumerate(list_file):
        if val['name'] == filename:
            file_id = val['id']
            exist_flag = 1
            break
        
    if exist_flag == 1:
        util.drive_service.files().update(fileId = file_id, media_body = media).execute()
    else:
        util.drive_service.files().create(body = file_metadata, media_body = media, fields = 'id').execute()
    
       
def push_tempo(username = util.username):
    log = ''
    try:
        filename = '{}_{}'.format(util.username, datetime.now().strftime('%d-%b-%y').upper())
        note_file = 'note_' + filename + '.json'
        action_file = 'action_' + filename + '.json'
        todo_file = 'todo_' + filename + '.json'
        image_file = 'image_' + filename + '.gif'
            
        #Push note
        try:
            push_file(note_file, util.path_note + note_file, util.tempo_id)
            log += 'Uploading note.\n'
        except:
            log += 'Note uploaded failed!\n'
        #Push action
        try:
            push_file(action_file, util.path_note + action_file, util.tempo_id)
            log += 'Uploading action.\n'
        except:
            log += 'Action uploaded failed!\n'
        #Push todo
        try:
            push_file(todo_file, util.path_note + todo_file, util.tempo_id)
            log += 'Uploading todo.\n'
        except:
            log += 'Todo uploaded failed!\n'
        #Push image
        try:
            push_file(image_file, util.path_note + image_file, util.tempo_id)
            log += 'Uploading image.\n'
        except:
            log += 'Image uploaded failed!\n'
        log += 'DONE.'
    except:
        pass
    
    return log
    
    
def get_current_status(username):
    _, temp_id = get_tempo_folder(username)
    list_file = lib_sys.list_file_by_folderid(temp_id)
    monitor_path = util.path_monitor + username + '/'
    if not os.path.exists(monitor_path):
        os.makedirs(monitor_path)
    
    for val in list_file:
        lib_sys.get_file_by_fileid(val, monitor_path)
    
    
    