import PySimpleGUI as sg
import os, sys
import pickle
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pygsheets



#Tempo
timer_popup = 60*5
timer_action = 60*60
timer_exist = 60*4
timer_monitor = 30
timer_thumbsup = 60*4

tempodata_id = '1yVbRDVhI_TLidjJXfM2I8KQp6zJZWEHn'
path_note = './modules/tempo/note/'
path_monitor = './modules/tempo/monitor/'
path_log = './log/'
path_config = './config/'
path_output = './output/'
path_browser = './modules/bot/driver/'
path_media = os.getcwd() + '/modules/bot/media/'

temponote_pid = None
capimgs_pid = None
tempomonitor_pid = None

    
def load_userdata():        
    filesize = os.path.getsize('user.txt')
    if filesize == 0:
        user_data = open('user.txt', 'w')
        username = input('Enter your username (gmailID_Alias, ex: tech01.cmt_Aiden): ')
        username = username.replace('\n', '')
        user_data.write(username)
        user_data.close()

    else:
        user_data = open('user.txt', 'r')
        username = user_data.readline()
        username = username.replace('\n', '')
        user_data.close()
        
    return username


def init_platform(fontsize = 13, theme = 'TanBlue'):
    sg.theme(theme)
    if sys.platform != 'win32':
        sg.set_options(font = ('Helvetica', fontsize))
                             

def get_credentials():
    creds = None
    # If modifying these scopes, delete the file token.pickle.'
    SCOPES = ['https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.file',
        'https://www.googleapis.com/auth/spreadsheets',
    ]
    
    try:
        if os.path.exists(path_config + 'token.pickle'):
            with open('./config/token.pickle', 'rb') as token:
                creds = pickle.load(token)
                
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
                
            else:
                flow = InstalledAppFlow.from_client_secrets_file(path_config + 'credentials.json', SCOPES)
                creds = flow.run_local_server(port = 0)
                
            # Save the credentials for the next run
            with open(path_config + 'token.pickle', 'wb') as token:
                pickle.dump(creds, token)
                
        return creds
        
    except Exception:
        return Exception


def list_folder_by_folderid(folder_id):
    results = drive_service.files().list(q = "mimeType='application/vnd.google-apps.folder' and parents in '"\
        + folder_id + "' and trashed = false", fields = "nextPageToken, files(id, name)", pageSize = 400).execute()
    items = results.get('files', [])
        
    return items  


def get_tempo_folder(username):
    tempo_id = ''
    temp_id = ''
    list_folder = list_folder_by_folderid(tempodata_id)
    for idx, val in enumerate(list_folder):
        if val['name'] == username:
            tempo_id = val['id']
            break
    if tempo_id != '':
        list_user_folder = list_folder_by_folderid(tempo_id)
        for idx, val in enumerate(list_user_folder):
            if val['name'] == 'temp_{}'.format(username):
                temp_id = val['id']
                break
        if temp_id == '':
            file_metadata = {
                'name': 'temp_{}'.format(username),
                'mimeType': 'application/vnd.google-apps.folder',
                'parents': [tempo_id]
            }
            temp_id = drive_service.files().create(body = file_metadata, fields = 'id').execute()['id']
    else:
        #Create new folder for user if not exist 
        file_metadata = {
            'name': username,
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [tempodata_id]
        }
        tempo_id = drive_service.files().create(body = file_metadata, fields = 'id').execute()['id']
        #Create temp folder
        file_metadata = {
            'name': 'temp_{}'.format(username),
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [tempo_id]
        }
        temp_id = drive_service.files().create(body = file_metadata, fields = 'id').execute()['id']
    
    return tempo_id, temp_id


drive_service = build('drive', 'v3', credentials = get_credentials())
ss_service = build('sheets', 'v4', credentials = get_credentials())
gc = pygsheets.authorize(custom_credentials = get_credentials())
                     
username = load_userdata()  
tempo_id, temp_id = get_tempo_folder(username)

