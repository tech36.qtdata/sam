import PySimpleGUI as sg
import os, sys
import traceback
import argparse
from datetime import datetime
import time
import json
import pyscreenshot as ImageGrab
import cv2
import base64
import imageio
from io import BytesIO
from PIL import Image, ImageTk
import shutil
import pandas as pd


import util
from lib import lib_tempo, lib_sys
from logger import log_temponote, log_tempomonitor



parser = argparse.ArgumentParser('Tempo Popup')
parser.add_argument('-c', '--capture-images', help = 'Capture images', default = 'off')
parser.add_argument('-n', '--popup-note', help = 'Run Tempo Note', default = 'off')
parser.add_argument('-m', '--popup-monitor', help = 'Run Tempo Monitor', default = 'off')
args = parser.parse_args()


def encode_b64(image_pil):        
    if image_pil.mode != 'RGB':
        image_pil = image_pil.convert('RGB')
    buff = BytesIO()
    image_pil.save(buff, format = 'png')
    image_b64 = base64.b64encode(buff.getvalue()).decode('utf-8')

    return image_b64
    

def snapshot():
    #Grabbing images
    try:
        screen = ImageGrab.grab()
        try:
            if sys.platform == 'win32':
                video_capture = cv2.VideoCapture(0 + cv2.CAP_DSHOW)
            else:
                video_capture = cv2.VideoCapture(0)
                
            #Fix black screen issue
            video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
            video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
                
            ret, frame = video_capture.read()
            time.sleep(1)
            video_capture.release()
                        
            #Convert to PIL Image
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = Image.fromarray(frame)
            
        except:
            frame = screen
#            log_temponote.warning(traceback.format_exc())
        
        #Resize
#        frame = frame.resize((100, 100), Image.ANTIALIAS)
#        screen = screen.resize((100, 100), Image.ANTIALIAS)
        #Encode to string
        frame_b64 = encode_b64(frame)
        screen_b64 = encode_b64(screen)
        
    except:
        log_temponote.warning('Cannot capture images!')
        log_temponote.warning(traceback.format_exc())
        frame_b64 = 'None'
        screen_b64 = 'None'
        
    return frame_b64, screen_b64

    
def capture_images(timer_monitor, temp_id):
    monitor_start = time.time()
    popup_first_run = True
    date = datetime.now().strftime('%d-%b-%y').upper()
    date_position = (10, 40)
    image_path = util.path_note + 'image_{}_{}.gif'.format(util.username, date)
    
    while True:
        try:
            #Capture image
            if time.time() > monitor_start + timer_monitor or popup_first_run:
                monitor_start = time.time()
                popup_first_run = False
                #Remove no use .gif
                try:
                    os.remove(util.path_note + 'new.gif')
                    os.remove(util.path_note + 'temp.gif')
                except:
                    pass

                frame_b64, screen_b64 = snapshot()
                                
                with imageio.get_writer(util.path_note + 'new.gif') as writer_screen:
                    screen = imageio.imread(base64.b64decode(screen_b64))
                    webcam = imageio.imread(base64.b64decode(frame_b64))
                    #crop webcame to overlay a webcame image on a screen image
                    crop_webcam = webcam[100:800, 350:1050]
                    crop_webcam = cv2.resize(crop_webcam, (300, 300))
                    x_offset = y_offset = 0
                    screen[y_offset:y_offset+crop_webcam.shape[0], x_offset:x_offset+crop_webcam.shape[1]] = crop_webcam
                    #concat_image = cv2.hconcat([crop_webcam, crop_webcam])
                    cv2.putText(
                        screen, #numpy array on which text is written
                        '{}'.format(date), #text
                        date_position, #position at which writing has to start
                        cv2.FONT_HERSHEY_SIMPLEX, #font family
                        1.3, #font size
                        (0, 0, 255), 3
                    )
                    #Image.save(image_path, save_all = True, append_images = screen)
                    writer_screen.append_data(screen)
                writer_screen.close()
                
                #Create writer object
                temp_gif = imageio.get_writer(util.path_note + 'temp.gif', duration = 1)
                try:
                    #Load old gif
                    old_gif = imageio.get_reader(image_path)
                    
                    for frame_number in range(old_gif.get_length()):
                        img = old_gif.get_next_data()
                        temp_gif.append_data(img)
                    old_gif.close()
                    
                except:
                    log_tempomonitor.warning(traceback.format_exc())
                    pass
                    
                #Load new gif 
                new_gif = imageio.get_reader(util.path_note + 'new.gif')        
                img = new_gif.get_next_data()
                temp_gif.append_data(img)
                
                new_gif.close()
                temp_gif.close()
                
                #Replace current by temp
                shutil.move(util.path_note + 'temp.gif', image_path)
                
                #Upload latest gif to temp folder                    
                lib_tempo.push_file('new.gif', util.path_note + 'new.gif', temp_id)

            time.sleep(timer_monitor)
                
        except:
            log_tempomonitor.warning(traceback.format_exc())
            pass
    
def create_window_note():
    util.init_platform(fontsize = 15, theme = 'TanBlue')
        
    layout = [
        [sg.Text('Custom Status')],
        [sg.Text(key = '-TIME-', size = (20, 1))],
        [sg.Text('Outcome', key = '-LABEL1-', size = (10, 1))],
        [sg.InputText(key = '-OUTCOME-', focus = True)],
        [sg.Text('Next Activity', key = '-LABEL2-')],
        [sg.InputText(key = '-NEXTACT-')],
        [sg.Text('Conclusion & Action', key = '-LABEL3-', visible = False)],
        [sg.InputText(key = '-ACTION-', visible = False)],
        [sg.Submit(key = '-SUBMIT-', bind_return_key = True)]
    ]
    
    window = sg.Window(
        'TempoNote 5000',
        layout,
        disable_close = True,
        keep_on_top = True,
        finalize = True
    )
    window.Element('-SUBMIT-').Update(visible = False)
    
    return window

    
def run_popup_note(timer_popup, timer_action, timer_exist, temp_id):
    #Init
    popup_start = time.time()
    action_start = time.time()
    action_is_on = False
    popup_first_run = True
    
    prev_act = ''
    prev_outcome = ''
    prev_action = ''
    while True:
        try:
            #Create note
            if time.time() > popup_start + timer_popup or popup_first_run:
                window = create_window_note()
                popup_start = time.time()
                popup_first_run = False
                
                current_time = datetime.now().strftime('%H:%M:%S')
                window.Element('-TIME-').Update('Time: {}'.format(current_time))
                
                if time.time() > action_start + timer_action:
                    action_is_on = True
                    action_start = time.time()
                    window.Element('-LABEL1-').Update('Outcomes')
                    window.Element('-LABEL2-').Update('Activities')
                    window.Element('-LABEL3-').Update(visible = True)
                    window.Element('-ACTION-').Update(visible = True)

                tp_act = 'None'
                tp_outcome = 'None'
                tp_action = 'None'
                while True:
                    event, values = window.read(timeout = 100)
                    if event == sg.TIMEOUT_KEY:
                        tp_outcome = values['-OUTCOME-']
                        tp_act = values['-NEXTACT-']
                        if action_is_on:    
                            tp_action = values['-ACTION-']
                        pass
                    
                    if time.time() > popup_start + timer_exist:
                        window.Hide()
                        window.Close()
                        break
                    
                    if action_is_on:
                        if tp_act is not None and tp_outcome is not None and tp_action is not None \
                        and len(tp_act) > 0 and len(tp_outcome) > 0 and len(tp_action) > 0 \
                        and event == '-SUBMIT-':
                            if prev_act != tp_act and prev_outcome != tp_outcome and prev_action != tp_action:
                                prev_act = tp_act
                                prev_outcome = tp_outcome
                                prev_action = tp_action
                                window.Hide()
                                window.Close()
                                break
                            else:
                                sg.popup('Action is duplicated. Please enter a new one.',
                                    location = (900, 300),
                                    keep_on_top = True)
                    else:
                        if tp_act is not None and len(tp_act) > 0 and event == '-SUBMIT-':
                            if prev_act != tp_act and prev_outcome != tp_outcome:
                                prev_act = tp_act
                                prev_outcome = tp_outcome
                                window.Hide()
                                window.Close()
                                break
                            else:
                                sg.popup('Note is duplicated. Please enter a new one.',
                                    location = (900, 300),
                                    keep_on_top = True)
                if action_is_on:
                    action_path = util.path_note + 'action_{}_{}.json'\
                        .format(util.username, datetime.now().strftime('%d-%b-%y').upper())
                    if not os.path.exists(action_path):
                        with open(action_path, 'w+') as file:
                            json.dump([], file)
                        json_action = []
                    else:
                        #Load json data     
                        with open(action_path, 'r') as file:
                            json_action = json.load(file)
                    
                    dict_action = {
                        'DATETIME': datetime.now().strftime('%d-%b-%y').upper(),
                        'TIME': current_time,
                        'NAME': util.username,
                        'ACTIVITIES': tp_act,
                        'OUTCOMES': tp_outcome,
                        'ACTION': tp_action,
                        'THUMBSUP': util.username
                    }
                            
                    #Write to note file
                    json_action.append(dict_action)
                    with open(action_path, 'w') as file:
                        json.dump(json_action, file)
                        
                    action_is_on = False
                
                else:
                    note_path = util.path_note + 'note_{}_{}.json'\
                        .format(util.username, datetime.now().strftime('%d-%b-%y').upper())
                    if not os.path.exists(note_path):
                        with open(note_path, 'w+') as file:
                            json.dump([], file)
                        json_note = []
                    else:
                        #Load json data     
                        with open(note_path, 'r') as file:
                            json_note = json.load(file)
                    
                    dict_note = {
                        'DATETIME': datetime.now().strftime('%d-%b-%y').upper(),
                        'TIME': current_time,
                        'NAME': util.username,
                        'OUTCOME': tp_outcome,
                        'NEXTACT': tp_act
                    }
                            
                    #Write to note file
                    json_note.append(dict_note)
                    with open(note_path, 'w') as file:
                        json.dump(json_note, file)
                        
                    #Upload latest note to temp folder
                    with open(util.path_note + 'temp_note.json', 'w') as fp:
                        json.dump(dict_note, fp)
                    
                    lib_tempo.push_file('temp_note.json', util.path_note + 'temp_note.json', temp_id)
                        
                time.sleep(timer_popup)
                        
        except UnicodeDecodeError:
            pass     
        
        except KeyboardInterrupt:
            pass
                        
        except:
            log_temponote.warning(traceback.format_exc())
            window.Close()
            break


def create_window_monitor(list_checkname):
    util.init_platform(fontsize = 13, theme = 'LightGreen')
    check_user = []

    for username in list_checkname:
        check_user.append([
            sg.Text(key = 'name_{}'.format(username), size = (25, 1))
        ])
        check_user.append([
            sg.Text('Task:', size = (5, 1)),
            sg.Text(key = 'task_{}'.format(username), size = (20, 1))
        ])
        check_user.append([
            sg.Text('Act:', size = (5, 1)),
            sg.Text(key = 'act_{}'.format(username), size = (20, 1))
        ])
        check_user.append([
            sg.Text('Outcome:', size = (5, 1)),
            sg.Text(key = 'outcome_{}'.format(username), size = (20, 1))
        ])
        check_user.append([
            sg.Image(key = 'image_{}'.format(username), size = (250, 160))
        ])
        
    if len(list_checkname) >= 2:
        layout = [
            [sg.Column(check_user, size = (300, 1000), vertical_scroll_only = True, scrollable = True)]
        ]
    else:
        layout = check_user
    
    window = sg.Window(
        'TempoMonitor',
        layout,
        location = (sg.Window.get_screen_size()[0] - 300, 0),
        keep_on_top = True,
        finalize = True,
        size = (300, 260),
        resizable = True
    )
    return window


def run_popup_monitor(timer_monitor):        
    try:
        list_checkname = get_list_checkname()
    except:
        sg.Popup('Error: list_checkname has no value.', title = 'Tempo Monitor', keep_on_top = True)
        
    window = create_window_monitor(list_checkname)
    monitor_start = time.time()
    popup_first_run = True

    while True:
        try:
            event, values = window.read(timeout = 100)
            if event == sg.TIMEOUT_KEY:
                if time.time() > monitor_start + timer_monitor or popup_first_run:
                    monitor_start = time.time()
                    popup_first_run = False
                    
                    for username in list_checkname:
                        try:
                            lib_tempo.get_current_status(username)
                        except:
                            continue
                        
                        user_path = util.path_monitor + username + '/'
                        try:
                            task = 'task1'
#                            task_content = 'Project: {}\n'\
#                                'Content: {}\n'\
#                                'Date: {}\n'\
#                                'Assigner: {}\n'\
#                                'Issue: {}\n'\
#                                'Solution: {}\n'\
#                                .format(df['PROJECT'][i], df['CONTENT'][i], df['DATE_MODIFIED'][i], \
#                                    df['ASSIGNER'][i], df['ISSUE'][i], df['SOLUTION'][i])
                            with open(user_path + 'temp_note.json', 'r') as file:
                                json_note = json.load(file)
                            act = json_note['NEXTACT']
                            outcome = json_note['OUTCOME']

                            window.Element('name_{}'.format(username)).Update(username)
                            window.Element('task_{}'.format(username)).Update(task)
#                            window.Element('doing_task{}'.format(username)).set_tooltip(task_content)
                            window.Element('act_{}'.format(username)).Update(act)
                            window.Element('outcome_{}'.format(username)).Update(outcome)
                            
                        except:
                            pass

                        try:
                            image = Image.open(user_path + 'new.gif')
                            image = image.resize((250, 160), Image.ANTIALIAS)
                            window.Element('image_{}'.format(username)).Update(data = ImageTk.PhotoImage(image))
                            
                        except:
                            continue
                continue
                    
            if event == sg.WIN_CLOSED:
                window.Close()
                break
                
        except UnicodeDecodeError:
            pass     
        
        except KeyboardInterrupt:
            pass
        
        except:
            log_tempomonitor.warning(traceback.format_exc())
            window.Close()
            break
        

def create_window_member(data, header):
    util.init_platform(fontsize = 13, theme = 'LightGreen')
    
    layout = [
        [sg.Button('Add'), sg.Button('Remove')],
        [sg.Table(
            key = '-TABLE-',
            values = data,
            headings = header,
            max_col_width = 25,
            auto_size_columns = True,
            display_row_numbers = True,
            justification = 'left',
            num_rows = 10,
            alternating_row_color = 'white',
            row_height = 35
        )],
        [sg.Output(key = '-OUT-', size = (29, 20))],
    ]
    
    window = sg.Window(
        'List of Member',
        layout,
        keep_on_top = True,
        size = (290, 500),
        resizable = True
    )
    
    return window
    
    
def get_list_checkname():
    df_checkname = pd.DataFrame(columns = ['CODE'])
    list_username = lib_sys.list_folder_by_folderid(util.tempodata_id)
    for idx, val in enumerate(list_username):
        df_checkname.loc[idx, 'CODE'] = val['name']
        
    data = df_checkname.values.tolist()
    header = df_checkname.columns.tolist()
    window = create_window_member(data, header)
    list_checkname = []
    
    while True:
        try:
            event, values = window.read()        
            if event == 'Add':
                member_index = values.get('-TABLE-')[0]
                list_checkname.append(df_checkname.loc[member_index, 'CODE'])
                list_checkname = list(set(list_checkname))
                window.Element('-OUT-').update('{}\n'.format(list_checkname))
                
            if event == 'Remove':
                member_index = values.get('-TABLE-')[0]
                try:
                    list_checkname.remove(df_checkname.loc[member_index, 'CODE'])
                except:
                    pass
                window.Element('-OUT-').update('{}\n'.format(list_checkname))
                
            if event == sg.WIN_CLOSED:
                window.close()
                break
            
        except UnicodeDecodeError:
            pass     
        
        except KeyboardInterrupt:
            pass
        
        except:
            log_temponote.warning(traceback.format_exc())
            window.Close()
            break
    
    return list_checkname


       
if args.capture_images == 'on':
    capture_images(util.timer_monitor, util.temp_id)
    
if args.popup_note == 'on':
    run_popup_note(util.timer_popup, util.timer_action, util.timer_exist, util.temp_id)

if args.popup_monitor == 'on':
    run_popup_monitor(util.timer_monitor)

